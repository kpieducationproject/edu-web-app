﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class LearnerGradeBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();

        public int Add(tbl_LearnerGrade tb)
        {
            tb.IsDeleted = false;
            db.tbl_LearnerGrade.Add(tb);
            db.SaveChanges();
            return tb.Id;
        }
    }
}