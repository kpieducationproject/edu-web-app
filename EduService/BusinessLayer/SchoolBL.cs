﻿using EduService.Models;
using EduService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class InstitutionBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public List<tbl_Institution> GetAllSchools()
        {
            var schools = db.tbl_Institution.Where(x =>  x.IsDeleted == false).ToList();

            return schools;
        }
        public tbl_Institution GetInstitution(int institutionId)
        {
            var institution = db.tbl_Institution.FirstOrDefault(x =>  x.IsDeleted == false&& x.Id == institutionId);

            return institution;
        }
        public List<InstitutionViewModel> GetAllCompanies(List<InstitutionViewModel> vmlist)
        {

            var companies = db.tbl_Institution.Where(x => x.IsDeleted == false).ToList();

            foreach (var item in companies)
            {
                InstitutionViewModel vm = new InstitutionViewModel();
                vm.Name = item.Name;
                vm.Id = item.Id;
                if (!string.IsNullOrWhiteSpace(item.Description))
                {
                    vm.Description = item.Description;
                }
                
                if (!string.IsNullOrWhiteSpace(item.Address))
                {
                    vm.Address = item.Address;
                }
                UsersBL userBL = new UsersBL();
                var users = userBL.GetInstitutionUsers(vm.Id);
                if (users != null && users.Count > 0)
                {
                    vm.CanDelete = false;
                }
                else
                {
                    vm.CanDelete = true;
                }
                vmlist.Add(vm);
            }

            return vmlist;
        }
        internal object IfInstitutionExists(string name)
        {
            return db.tbl_Institution.FirstOrDefault(x => x.Name.ToLower() == name.ToLower() &&  x.IsDeleted == false);
        }
        internal bool CheckIfInstitutionExists(string name)
        {
            bool status;

            var item = db.tbl_Institution.FirstOrDefault(x => x.Name.ToLower() == name.ToLower() &&  x.IsDeleted == false);

            if (item != null)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }
        internal int Add(InstitutionViewModel vm)
        {
            tbl_Institution institution = new tbl_Institution();
            institution.Name = vm.Name;
            if (!string.IsNullOrWhiteSpace(vm.Description))
            {
                institution.Description = vm.Description;
            }
   
            if (!string.IsNullOrWhiteSpace(vm.Address))
            {
                institution.Address = vm.Address;
            }
            institution.IsDeleted = false;
            db.tbl_Institution.Add(institution);
            db.SaveChanges();
            return institution.Id;
        }
        internal void Delete(int id)
        {
            var institution = db.tbl_Institution.FirstOrDefault(x => x.Id == id);

            if (institution != null)
            {
                institution.IsDeleted = true;
                db.SaveChanges();
            }
        }
    }
}