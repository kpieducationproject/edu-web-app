﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using EduService.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace EduService.BusinessLayer
{
    public class RoleBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();

        public List<AspNetRoles> GetRoles(string UserId)
        {
            List<AspNetRoles> items = new List<AspNetRoles>();
            var user = db.AspNetUsers.FirstOrDefault(x => x.Id == UserId && x.IsDeleted == false);
            if (user != null)
            {
                items = user.AspNetRoles.ToList();
            }

            return items;
        }
        public string GetRoleIdByName(string RoleName)
        {
            string roleId = string.Empty;
            var role = db.AspNetRoles.FirstOrDefault(x => x.Name.ToLower() == RoleName.ToLower());
            if (role != null)
            {
                roleId = role.Id;
            }
            return roleId;
        }
    }
}