﻿using EduService.Models;
using EduService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class ClassesBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public List<ClassesViewModel> GetClasses(int SchoolId)
        {
            List<ClassesViewModel> list = new List<ClassesViewModel>();

            var items = db.tbl_Class.Where(x => x.IsDeleted == false).ToList();
            foreach (var item in items)
            {
                ClassesViewModel vm = new ClassesViewModel();
                vm.Id = item.Id;
                vm.Capacity = item.Capacity;
                vm.Grade = item.tbl_Grades.Grade;
                vm.Division = item.tbl_Division.divisionDescription;
                vm.Name = item.Name;
                vm.DivisionId = item.DivisionId;
                list.Add(vm);
            }
            return list;
        }
        internal int Add(ClassesViewModel vm)
        {
            tbl_Class item = new tbl_Class();
            item.Name = vm.Name;
            item.Capacity = vm.Capacity;
            item.GradeId = vm.GradeId;
            item.SchoolId = vm.SchoolId;
            item.DivisionId = vm.DivisionId;
            item.IsDeleted = false;
            item.DateCreated = DateTime.Now;
            db.tbl_Class.Add(item);
            db.SaveChanges();
            foreach (var learnerId in vm.LearnerId)
            {
                tbl_LearnerClass leanerclass = new tbl_LearnerClass();
                leanerclass.ClassId = item.Id;
                leanerclass.UserId = learnerId;
                db.tbl_LearnerClass.Add(leanerclass);
                db.SaveChanges();
            }
            foreach (var teacherId in vm.TeacherId)
            {
                tbl_ClassTeacher teacher = new tbl_ClassTeacher();
                teacher.ClassId = item.Id;
                teacher.UserId = teacherId;
                db.tbl_ClassTeacher.Add(teacher);
                db.SaveChanges();
            }
            return item.Id;
        }
        internal bool Delete(int id)
        {
            var deleted = false;
            var item = db.tbl_Class.FirstOrDefault(x => x.Id == id);

            if (item != null)
            {
                item.IsDeleted = true;
                db.SaveChanges();
                deleted = true;
            }
            var learners = db.tbl_LearnerClass.Where(x => x.ClassId == id);
            foreach (var learner in learners)
            {
                learner.IsDeleted = true;
                db.SaveChanges();
            }
            var teachers = db.tbl_ClassTeacher.Where(x => x.ClassId == id);
            foreach (var teacher in teachers)
            {
                teacher.IsDeleted = true;
                db.SaveChanges();
            }
            return deleted;
        }

        public bool IsExists(string name, int gradeId, int schoolId)
        {
            var classExists = db.tbl_Class.FirstOrDefault(x => x.Name == name && x.GradeId == gradeId && x.SchoolId == schoolId);
            if (classExists != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<ApplicationUser> GetLearnersForClass(int gradeId, int schoolId, List<ApplicationUser> aspusers)
        {
            var learnersFromGrade = db.tbl_LearnerGrade.Where(x => x.AspNetUsers.InstitutionId == schoolId && x.AspNetUsers.IsDeleted == false && x.tbl_Grades.Id == gradeId && x.tbl_Grades.IsDeleted == false).ToList();
            List<ApplicationUser> learnersInGrade = new List<ApplicationUser>();
            foreach (var item in aspusers)
            {

                learnersFromGrade.Where(x => x.UserId == item.Id);
                learnersInGrade.Add(item);
            }
            return learnersInGrade;
        }
        public List<ApplicationUser> GetAllLearners(List<ApplicationUser> aspusers, string roleId)
        {
            List<ApplicationUser> learners = new List<ApplicationUser>();

            foreach (var item in aspusers)
            {
                if (item.IsDeleted == false)
                {
                    foreach (var role in item.Roles)
                    {
                        if (role.RoleId == roleId)
                        {
                            learners.Add(item);
                        }
                    }
                }
            }

            return learners;
        }
    }
}