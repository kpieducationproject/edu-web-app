﻿using EduService.Models;
using EduService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class SubjectBL
    {

        public EduServiceDBEntities db = new EduServiceDBEntities();
        //GET ALL SUBJECTS
        public List<tbl_Subjects> GetAllSubjects()
        {
            var subjects = db.tbl_Subjects.Where(x => x.isDeleted == false).ToList();
            return subjects;
        }




        //GET Subject by Id
        public tbl_Subjects GetSubject(int subjectId)
        {
            var subject = db.tbl_Subjects.FirstOrDefault(x => x.isDeleted == false && x.subjectId == subjectId);

            return subject;
        }

        //


        public List<SubjectViewModel> GetAllSubjectLists(List<SubjectViewModel> vmlist)
        {

            var subjects = db.tbl_Subjects.Where(x => x.isDeleted == false).ToList();
            
            foreach (var item in subjects)
            {
                SubjectViewModel vm = new SubjectViewModel();
                vm.subjectName = item.subjectName;
                vm.subjectCode = item.subjectCode;
                vm.isActive = item.isActive;
                vm.isDeleted = item.isDeleted;
                vm.CanDelete = true;

                
                vmlist.Add(vm);
            }

            return vmlist;
        }


        internal object IfSubjectExists(string name)
        {
            return db.tbl_Subjects.FirstOrDefault(x => x.subjectName.ToLower() == name.ToLower() && x.isDeleted == false);
        }

        internal bool CheckIfSubjectExists(string name)
        {
            bool status;

            var item = db.tbl_Subjects.FirstOrDefault(x => x.subjectName.ToLower() == name.ToLower() && x.isDeleted == false);

            if (item != null)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }

        //Add
        internal int Add(SubjectViewModel vm)
        {
            tbl_Subjects subject = new tbl_Subjects();
            subject.subjectName = vm.subjectName;
            subject.subjectCode = vm.subjectCode;
            subject.isActive = true;
            subject.isDeleted = false;
            db.tbl_Subjects.Add(subject);
            db.SaveChanges();
            return subject.subjectId;
        }

        internal void Delete(int id)
        {
            var subject = db.tbl_Subjects.FirstOrDefault(x => x.subjectId == id);

            if (subject != null)
            {
                subject.isActive = false;
                subject.isDeleted = true;
                db.SaveChanges();
            }
        }

    }
}