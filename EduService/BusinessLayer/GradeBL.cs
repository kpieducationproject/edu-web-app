﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class GradeBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();


        public List<tbl_Class> GetAllClasses()
        {
            var _classes = db.tbl_Class.Where(x => x.IsDeleted == false).ToList();
            return _classes;
        }


        public tbl_Class GetClasses(int id)
        {
            tbl_Class items = new tbl_Class();
            var lesson = db.tbl_Timetable.FirstOrDefault(x => x.Id == id);
            if(lesson != null)
            {
                items = lesson.tbl_Class;
            }
            return items;
        }


    }
}