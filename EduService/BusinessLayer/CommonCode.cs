﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Text;
using System.Net.Http.Headers;
using EduService.Controllers;

namespace EduService.BusinessLayer
{
    public static class CommonCode
    {

        public static HttpClient CreateClientInstance()
        {
            var authData = string.Format("{0}:{1}", "", "");
            var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = int.MaxValue;
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
            return client;
        }
        public static char FirstLetterToUpper(string str)
        {
            var firstletter = 'X';

            if (str == null)
            {
                firstletter = 'X';
            }

            if (str.Length > 1)
            {
                firstletter = char.ToUpper(str[0]);
            }

            return firstletter;
        }

    }
}