﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EduService.ViewModels;
using EduService.Controllers;

namespace EduService.BusinessLayer
{

    public class UsersBL
    {
        IdentitiyController identityController = new IdentitiyController();
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public Cryptography _cryptography = new Cryptography();
        InstitutionBL institutionBL = new InstitutionBL();

        public AspNetUsers FindUserById(string UserId)
        {
            return db.AspNetUsers.FirstOrDefault(x => x.Id == UserId && x.IsDeleted == false);
        }
        public AspNetUsers FindUserByIdNumber(string IdNumber)
        {
            return db.AspNetUsers.FirstOrDefault(x => x.IdNumber == IdNumber && x.IsDeleted == false);
        }        public AspNetUsers FindUserByEmail(string email)
        {
            return db.AspNetUsers.FirstOrDefault(x => x.Email.ToLower() == email && x.IsDeleted == false);
        }
        public List<AspNetUsers> GetAllUsers()
        {
            return db.AspNetUsers.Where(x => x.IsDeleted == false).ToList();
        }
        public List<AspNetUsers> GetInstitutionUsers(int institutionId)
        {
            return db.AspNetUsers.Where(x => x.InstitutionId == institutionId && x.IsDeleted == false).ToList();
        }
        public List<UserViewModel> GetUsers(string userRole, int institutionId)
        {
            List<UserViewModel> users = new List<UserViewModel>();
            var aspusers = new List<AspNetUsers>();
            if (userRole == "System Admin")
            {
                aspusers = GetAllUsers();
            }
            else
            {
                aspusers = GetInstitutionUsers(institutionId);
            }

            if (aspusers != null && aspusers.Count > 0)
            {
                var schools = institutionBL.GetAllSchools();

                foreach (var item in aspusers)
                {
                    var viewmodel = new UserViewModel()
                    {
                        Email = item.Email,
                        FirstName = item.FirstName,
                        FullName = item.FirstName + " " + item.LastName,
                        IsDeleted = item.IsDeleted,
                        Username = item.UserName,
                        LastName = item.LastName,
                        UserId = item.Id,
                        SchoolId = item.InstitutionId,
                        Suburb = item.Suburb,
                        StreetAddress = item.StreetAddress,
                        IdNumber = item.IdNumber,
                        PhoneNumber = item.PhoneNumber,
                        EmailConfirmed = item.EmailConfirmed
                    };
                    var institution = schools.FirstOrDefault(x => x.Id == item.InstitutionId);
                    if (institution != null)
                    {
                        viewmodel.InstitutionName = institution.Name;
                    }
                    if (item.HomePhone != null)
                    {
                        viewmodel.HomePhone = item.HomePhone;
                    }    //if (userRole != "System Admin")
                    //{
                    users.Add(viewmodel);
                    //}
                }
            }

            return users;
        }

        public UserViewModel GetUserFullName(string UserId)
        {
            UserViewModel userswithFullNames = new UserViewModel();
            var user = db.AspNetUsers.FirstOrDefault(x => x.Id == UserId);

            var userwithFullName = new UserViewModel()
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
            };

            return userwithFullName;
        }

        public List<UserViewModel> GetUsers(string role, string userId)
        {
            List<UserViewModel> userlist = new List<UserViewModel>();
            var senders = new List<AspNetUsers>();
            if (role == "System Admin")
            {
                senders = GetAllUsers();

      
            }
            else
            {
                var user = GetUser(userId);
                if (user != null && user.InstitutionId > 0)
                {
                    senders = GetInstitutionUsers(user.InstitutionId);
                }
            }
            var schools = institutionBL.GetAllSchools();

            foreach (var sender in senders)
            {
                var senderFullDetails = new UserViewModel()
                {
                    SenderId = sender.Id,
                    UserId = sender.Id,
                    FullName = sender.FirstName + " " + sender.LastName,
                    FirstName = sender.FirstName,
                    LastName = sender.LastName,
                    Email = sender.Email,
                    SchoolId = sender.InstitutionId,
                    Suburb = sender.Suburb,
                    PhoneNumber = sender.PhoneNumber,
                    StreetAddress = sender.StreetAddress,
                    IdNumber = sender.IdNumber
                };
                var institution = schools.FirstOrDefault(x => x.Id == sender.InstitutionId);
                if (institution != null && !string.IsNullOrWhiteSpace(institution.Name))
                {
                    senderFullDetails.InstitutionName = institution.Name;
                }
                if (sender.HomePhone != null && !string.IsNullOrWhiteSpace(sender.HomePhone))
                {
                    senderFullDetails.HomePhone = sender.HomePhone;
                }
                userlist.Add(senderFullDetails);
            }

            return userlist;
        }
        public bool IsIdNumExists(string IdNumber)
        {
            bool status;

            var user = db.AspNetUsers.FirstOrDefault(x => x.IdNumber == IdNumber && x.IsDeleted == false);

            if (user != null)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }
        public bool IsUsernameExists(string email)
        {
            bool status;

            var user = db.AspNetUsers.FirstOrDefault(x => x.Email.ToLower() == email.ToLower() && x.IsDeleted == false);

            if (user != null)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }

        public bool IsUpdateEmailExists(string email, string userId)
        {
            bool status;

            var user = db.AspNetUsers.FirstOrDefault(x => x.Email.ToLower() == email.ToLower() && x.IsDeleted == false && x.Id != userId);

            if (user != null)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }
        public bool IsNewIdExists(string IdNumber, string userId)
        {
            bool status;

            var user = db.AspNetUsers.FirstOrDefault(x => x.IdNumber == IdNumber && x.IsDeleted == false && x.Id != userId);

            if (user != null)
            {
                status = false;
            }
            else
            {
                status = true;
            }
            return status;
        }
        public void Delete(string id)
        {
            var user = db.AspNetUsers.FirstOrDefault(x => x.Id == id);

            if (user != null)
            {
                user.IsDeleted = true;
                db.SaveChanges();
            }
        }

        internal AspNetUsers GetUser(string id)
        {
            return db.AspNetUsers.FirstOrDefault(x => x.Id == id);
        }
        internal bool DeletedUserExists(string IdNumber)
        {
            bool IsUserExists = false;
            var user = db.AspNetUsers.FirstOrDefault(x => x.IdNumber == IdNumber && x.IsDeleted == true);
            if (user != null && !string.IsNullOrWhiteSpace(user.Id))
            {
                IsUserExists = true;
            }
            return IsUserExists;
        }
        public bool UserExists(string IdNumber)
        {
            bool IsUserExists = false;

            var user = db.AspNetUsers.FirstOrDefault(x => x.IdNumber == IdNumber);

            if (user != null && !string.IsNullOrWhiteSpace(user.Id))
            {
                IsUserExists = true;
            }
            return IsUserExists;
        }
        internal void UpdateInstitution(string userId, int institutionId)
        {
            var user = db.AspNetUsers.FirstOrDefault(x => x.Id == userId);

            if (user != null)
            {
                user.InstitutionId = institutionId;
                db.SaveChanges();
            }
        }
        internal bool UpdateProfile(ProfileViewModel user)
        {
            bool updated = false;

            var item = db.AspNetUsers.FirstOrDefault(x => x.Id == user.UserId);
            if (item != null && !string.IsNullOrWhiteSpace(item.Id))
            {
                item.FirstName = user.FirstName;
                item.Email = user.Email;
                item.UserName = user.Email;
                item.LastName = user.LastName;
                item.InstitutionId = user.SchoolId;
                item.Suburb = user.Suburb;
                item.PhoneNumber = user.PhoneNumber;
                item.StreetAddress = user.StreetAddress;
                item.IdNumber = user.IdNumber;
                item.IsDeleted = false;
                if (user.Password != null && !string.IsNullOrWhiteSpace(user.Password))
                {
                    item.PasswordHash = user.Password;
                }
                if (user.HomePhone != null && !string.IsNullOrWhiteSpace(user.HomePhone))
                {
                    item.HomePhone = user.HomePhone;
                }
                else
                {
                    user.HomePhone = "";
                }


                db.SaveChanges();
                updated = true;
            }

            return updated;
        }
        internal bool EditUser(EditUserViewModel user)
        {
            bool updated = false;

            var item = db.AspNetUsers.FirstOrDefault(x => x.Id == user.UserId);
            if (item != null && !string.IsNullOrWhiteSpace(item.Id))
            {
                item.FirstName = user.FirstName;
                item.LastName = user.LastName;
                item.InstitutionId = user.SchoolId;
                item.Suburb = user.Suburb;
                item.PhoneNumber = user.PhoneNumber;
                item.StreetAddress = user.StreetAddress;
                item.IdNumber = user.IdNumber;
                item.IsDeleted = false;

                if (user.HomePhone != null && !string.IsNullOrWhiteSpace(user.HomePhone))
                {
                    item.HomePhone = user.HomePhone;
                }
                else
                {
                    user.HomePhone = "";
                }


                db.SaveChanges();
                updated = true;
            }

            return updated;
        }
        internal bool Update(UserViewModel user)
        {
            bool updated = false;

            var item = db.AspNetUsers.FirstOrDefault(x => x.Id == user.UserId);
            if (item != null && !string.IsNullOrWhiteSpace(item.Id))
            {
                item.FirstName = user.FirstName;
                item.LastName = user.LastName;
                item.InstitutionId = user.SchoolId;
                item.Suburb = user.Suburb;
                item.PhoneNumber = user.PhoneNumber;
                item.StreetAddress = user.StreetAddress;
                item.IdNumber = user.IdNumber;
                item.IsDeleted = false;
                if (user.Password != null && !string.IsNullOrWhiteSpace(user.Password))
                {
                    item.PasswordHash = user.Password;
                }
                if (user.HomePhone != null && !string.IsNullOrWhiteSpace(user.HomePhone))
                {
                    item.HomePhone = user.HomePhone;
                }
                else
                {
                    user.HomePhone = "";
                }


                db.SaveChanges();
                updated = true;
            }

            return updated;
        }

        internal bool VerifyUser(SetPasswordViewModel vm)
        {
            bool updated = false;

            var item = db.AspNetUsers.FirstOrDefault(x => x.Email.ToLower() == vm.Email.ToLower() && x.IsDeleted == false);
            if (item != null)
            {
                item.EmailConfirmed = true;
                item.PasswordHash = vm.Password;
                db.SaveChanges();
                updated = true;
            }

            return updated;
        }

    }
}
