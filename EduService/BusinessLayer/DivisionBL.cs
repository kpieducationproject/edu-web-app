﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class DivisionBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public List<tbl_Division> GetAllDivisions()
        {
            var divisions = db.tbl_Division.Where(x => x.isDeleted == false).ToList();
            return divisions;
        }
    }
}