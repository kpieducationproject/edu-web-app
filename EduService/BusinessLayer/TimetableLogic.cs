﻿using EduService.Models;
using EduService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace EduService.BusinessLayer
{
    public class TimetableLogic
    {

        public EduServiceDBEntities db = new EduServiceDBEntities();
        GradeBL objGradeLogic = new GradeBL();
        SubjectBL objSubjectLogic = new SubjectBL();
        UsersBL objUserLogic = new UsersBL();


        public string classToString(tbl_Class _class)
        {
            return _class.tbl_Grades.Grade + "" + _class.tbl_Division.divisionDescription;
        }

        public tbl_Timetable findLessonById(int lessonId)
        {
            return db.tbl_Timetable.FirstOrDefault(x => x.Id == lessonId);
        }

        internal object IfLessonExists(string name)
        {
            return db.tbl_Timetable.FirstOrDefault(x => x.timetableDescription == name);
        }


        public int Create(TimetableViewModel lesson)
        {
            tbl_Timetable newLesson = new tbl_Timetable();

            newLesson = new tbl_Timetable
            {
                timetableDescription = lesson.timetableDescription,
                dayoftheweek = lesson.dayofweek,
                lessonTime = lesson.lessonTime,
                ClassId = lesson.classId,
                subjectId = lesson.subjectId,
                teacherId = lesson.userId
            };
            db.tbl_Timetable.Add(newLesson);
            db.SaveChanges();
            return newLesson.Id;
        }

        public bool Edit(TimetableViewModel lessonvm)
        {
            bool updated = false;

            var lesson = db.tbl_Timetable.FirstOrDefault(x => x.Id == lessonvm.timetableId);

            if (lesson != null)
            {
                lesson.timetableDescription = lessonvm.timetableDescription;
                lesson.dayoftheweek = lessonvm.dayofweek;
                lesson.lessonTime = lessonvm.lessonTime;
                lesson.ClassId = lessonvm.classId;
                lesson.subjectId = lessonvm.subjectId;
                lesson.teacherId = lessonvm.userId;

                db.SaveChanges();
                updated = true;
            }
            return updated;
        }

        public List<TimetableViewModel> getFullTimetable()
        {
            var fullTable = new List<TimetableViewModel>();
            var time = db.tbl_Timetable.ToList();
            var allgrades = objGradeLogic.GetAllClasses();
            var allsubjects = objSubjectLogic.GetAllSubjects();
            var allusers = objUserLogic.GetAllUsers();


            foreach (var item in time)
            {
                TimetableViewModel temp = new TimetableViewModel()
                {
                    timetableId = item.Id,
                    timetableDescription = item.timetableDescription,
                    lessonTime = item.lessonTime,
                    dayofweek = item.dayoftheweek,
                    classId = item.tbl_Class.Id,
                    subjectId = item.tbl_Subjects.subjectId,
                    userId = item.teacherId
                };

                var grade = allgrades.FirstOrDefault(x => x.Id == temp.classId);
                if (grade != null)
                {
                    temp.Class = grade.Name;
                }
                var subject = allsubjects.FirstOrDefault(x => x.subjectId == temp.subjectId);
                if (subject != null)
                {
                    temp.Subject = subject.subjectName;
                }

                var teacher = allusers.FirstOrDefault(x => x.Id == temp.userId);
                if (teacher != null)
                {
                    temp.Teacher = teacher.FirstName.Substring(0, 1) + " " + teacher.LastName;
                }


                fullTable.Add(temp);
            }

            return fullTable;
        }

        public List<TimetableViewModel> getTimetablebyclass(tbl_Class _class, List<TimetableViewModel> myTable)
        {
            var myClass = classToString(_class);

            if (myClass != null)
            {
                var temp_table = getFullTimetable();

                foreach (var item in temp_table)
                {

                    tbl_Class x = db.tbl_Class.Find(item.classId);

                    if (classToString(x) == myClass)
                    {
                        TimetableViewModel tempTimeTable = new TimetableViewModel
                        {
                            timetableId = item.timetableId,
                            timetableDescription = item.timetableDescription,
                            lessonTime = item.lessonTime,
                            dayofweek = item.dayofweek,
                            Classes = item.Classes,
                            Subjects = item.Subjects,
                            userId = item.userId
                        };
                        myTable.Add(tempTimeTable);
                    }

                }
            }
            return myTable;
        }

        public List<TimetableViewModel> getTimetableBySubject(tbl_Subjects _subject, List<TimetableViewModel> myTable)
        {
            if (_subject != null)
            {
                var temp_table = getFullTimetable();

                foreach (var item in temp_table)
                {
                    tbl_Subjects x = db.tbl_Subjects.Find(item.subjectId);

                    if (x != null)
                    {
                        TimetableViewModel tempTimeTable = new TimetableViewModel
                        {
                            timetableId = item.timetableId,
                            timetableDescription = item.timetableDescription,
                            lessonTime = item.lessonTime,
                            dayofweek = item.dayofweek,
                            Classes = item.Classes,
                            Subjects = item.Subjects,
                            userId = item.userId
                        };
                        myTable.Add(tempTimeTable);
                    }

                }
            }
            return myTable;
        }

        public List<TimetableViewModel> getTimetableByTeacher(string teacherId, List<TimetableViewModel> myTable)
        {
            var teacher = db.AspNetUsers.Find(teacherId);
            if (teacher != null)
            {
                var temp_table = getFullTimetable();

                foreach (var item in temp_table)
                {
                    if (item.userId == teacherId)
                    {
                        TimetableViewModel tempTimeTable = new TimetableViewModel
                        {
                            timetableId = item.timetableId,
                            timetableDescription = item.timetableDescription,
                            lessonTime = item.lessonTime,
                            dayofweek = item.dayofweek,
                            Classes = item.Classes,
                            Subjects = item.Subjects,
                            userId = item.userId
                        };
                        myTable.Add(tempTimeTable);
                    }

                }
            }
            return myTable;
        }

    }
}