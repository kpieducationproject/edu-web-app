﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class GradesBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public List<tbl_Grades> GetGrades()
        {
            var grades = db.tbl_Grades.Where(x => x.IsDeleted == false).ToList();

            return grades;
        }
    }
}