﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EduService.BusinessLayer
{
    public class LinkExpirationBL
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public tbl_LinkExpiration GetActivationLink(string userId)
        {
            var item = db.tbl_LinkExpiration.FirstOrDefault(x => x.IsDeleted == false && x.UserId == userId);

            return item;
        }

        internal int Add(tbl_LinkExpiration obj)
        {
            obj.DateCreated = DateTime.Now;
            obj.IsDeleted = false;
            obj.IsActive = true;
            db.tbl_LinkExpiration.Add(obj);
            db.SaveChanges();
            return obj.Id;
        }

        internal void Delete(int id)
        {
            var obj = db.tbl_LinkExpiration.FirstOrDefault(x => x.Id == id);

            if (obj != null)
            {
                db.tbl_LinkExpiration.Remove(obj);
                db.SaveChanges();
            }
        }
        public tbl_LinkExpiration CheckIfExists(string UserId)
        {
            tbl_LinkExpiration exists = new tbl_LinkExpiration();
            var obj = db.tbl_LinkExpiration.FirstOrDefault(x => x.UserId == UserId);

            if (obj != null)
            {
                exists = obj;
            }
            return exists;
        }
    }
}