﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class TimetableViewModel
    {
        public int timetableId { get; set; }

        [Display(Name = "Description")]
        public string timetableDescription { get; set; }
        
        [Required(ErrorMessage = "Enter time of Lesson")]
        [Display(Name = "Lesson Time")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = @"{0:hh\:mm}")]
        public TimeSpan? lessonTime { get; set; }
        
        [Required(ErrorMessage = "Enter the Day")]
        [Display(Name = "Day")]
        public string dayofweek { get; set; }
        
        [Required(ErrorMessage = "Enter the class")]
        [Display(Name = "Classes")]
        public int classId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }

        [Required(ErrorMessage = "Select a Subject")]
        [Display(Name = "Subjecsts")]
        public int subjectId { get; set; }
        public IEnumerable<SelectListItem> Subjects { get; set; }

        [Required(ErrorMessage = "Select a Teacher")]
        [Display(Name = "Teachers")]
        public string userId { get; set; }
        public IEnumerable<SelectListItem> Teachers { get; set; }

        [Display(Name = "Class")]
        public string Class { get; internal set; }

        [Display(Name = "Subject")]
        public string Subject { get; internal set; }

        [Display(Name = "Teacher")]
        public string Teacher { get; internal set; }
    }
}