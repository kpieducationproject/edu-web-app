﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class SubjectViewModel
    {
        public int subjectId { get; set; }

        [Required(ErrorMessage = "Subject name is required")]
        [DisplayName("Subject Name")]
        [Remote("IsSubjectExists", "Subjects", HttpMethod = "POST", ErrorMessage = "Subject already exists")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string subjectName { get; set; }

        [DisplayName("Subject Code")]
        public string subjectCode { get; set; }

        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isDeleted { get; set; }
        public bool CanDelete { get; set; }
    }
}