﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace EduService.ViewModels
{
    public class CreateUserViewModel
    {
        [Display(Name = "User Role")]
        [Required(ErrorMessage = "User Role is required")]
        public string RoleId { get; set; }
        public IEnumerable<SelectListItem> UserRoles { get; set; }

        [Display(Name = "Institution")]
        public int InstitutionId { get; set; }
        public IEnumerable<SelectListItem> Companies { get; set; }


        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [MaxLength(255)]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Invalid email format")]
        [Remote("IsUsernameExists", "Account", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [RegularExpression(@"^[a-zA-Z0-9\s]{6,}$", ErrorMessage = "Password length must be at least 6 characters")]
        [StringLength(255, ErrorMessage = "Must be between 6 and 255 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm password is required")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [MaxLength(10, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"^(\+\d{1,3}[- ]?)?\d{10}$", ErrorMessage = "Invalid phone number format")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string UserId { get; set; }
        public bool IsDeleted { get; set; }
    }
}