﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class InstitutionViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Institution name is required")]
        [DisplayName("Institution Name")]
        [Remote("IsInstitutionExists", "Institution", HttpMethod = "POST", ErrorMessage = "Institution already exists")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string Name { get; set; }

        public string Description { get; set; }
        [DisplayName("Registration Number")]
        public string RegNo { get; set; }
        public string Address { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public bool CanDelete { get; set; }
    }
}