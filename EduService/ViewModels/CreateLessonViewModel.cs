﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class CreateLessonViewModel
    {
        
        public string timetableDescription { get; set; }

        [Required(ErrorMessage = "Enter time of Lesson")]
        [Display(Name = "Lesson Time")]
        public TimeSpan? lessonTime { get; set; }

        [Required(ErrorMessage = "Enter the Day")]
        [Display(Name = "Day")]
        public string dayofweek { get; set; }

        [Display(Name = "Class")]
        public string classId { get; set; }
        public IEnumerable<SelectListItem> Classes { get; set; }

        [Display(Name = "Subjects")]
        public int subjectId { get; set; }
        public IEnumerable<SelectListItem> Subjects { get; set; }
        
        [Display(Name = "Teachers")]
        public string userId { get; set; }
        public IEnumerable<SelectListItem> Teachers { get; set; }

    }
}