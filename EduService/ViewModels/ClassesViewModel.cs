﻿using EduService.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class ClassesViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Class name is required")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [Display(Name = "Class Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Grade is required")]
        [Display(Name = "Grade")]
        public int GradeId { get; set; }
        public IEnumerable<SelectListItem> Grades { get; set; }

        [Required(ErrorMessage = "Subject is required")]
        [Display(Name = "Division")]
        public int DivisionId { get; set; }
        public IEnumerable<SelectListItem> Divisions { get; set; }

        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Count must be a natural number")]
        [Required(ErrorMessage = "Capacity is required")]
        [Display(Name = "Capacity")]
        public int Capacity { get; set; }

        [Required(ErrorMessage = "Learners are required")]
        [Display(Name = "Learners")]
        public string[] LearnerId { get; set; }
        public IEnumerable<SelectListItem> Learners { get; set; }
        [Required(ErrorMessage = "Teacher(s) is required")]
        [Display(Name = "Teacher")]
        public string[] TeacherId { get; set; }
        public IEnumerable<SelectListItem> Teachers { get; set; }

        public Nullable<bool> IsDeleted { get; set; }
        public bool CanDelete { get; set; }
        public int SchoolId { get; set; }

        public string Grade { get; set; }
        public string Division { get; set; }
        public string Subject { get; set; }
        public string LearnerName { get; set; }
 
    }
}