﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class FileViewModel
    {
        public IList<AttachmentViewModel> Attachments { get; set; }
        public IList<HttpPostedFileBase> fileUpload { get; set; }

        public string FilesToBeUploaded { get; set; }
        //[RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.pdf)$", ErrorMessage = "Only pdf files allowed.")]
        [Required(ErrorMessage = "Please select file")]
        public IList<HttpPostedFileBase> Files { get; set; }
        public string Base64 { get; set; }
        [DisplayName("Recipient")]
        public bool UseOnceOff { get; set; }
        [DisplayName("Include Signature")]
        public bool IncludeSignature { get; set; }     
        //[DisplayName("Manually Capture Recipient")]
        //public bool UseSearch { get; set; }
        public string ShowManualCapture { get; set; }
        public string ShowReceipient { get; set; }
        public string Name { get; set; }
        public string SenderDisplay { get; set; }
        public string ContentType { get; set; }
        [Required(ErrorMessage = "Sender is required")]
        [DisplayName("Search Sender")]
        public string SenderId { get; set; }
        public IEnumerable<SelectListItem> Senders { get; set; }

        [Required(ErrorMessage = "Recipient is required")]
        [DisplayName("Search Recipient")]
        public int[] ContactId { get; set; }

        public IEnumerable<SelectListItem> Contacts { get; set; }
        [DisplayName("Subject")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string Subject { get; set; }
        [DisplayName("Body")]
        public string Body { get; set; }
        [DisplayName("Sent By")]
        public string SentBy { get; set; }
        public string SentByFirstName { get; set; }
        public string SentByLastName { get; set; }
        public DateTime DateCreated { get; set; }
        public string UnlockCode { get; set; }
        public string FullName { get; set; }
        public string FullNameEmail
        {
            get { return FirstName + " " + LastName + " - " + Email; }
            set { }
        }
        [Display(Name = "1st Additional Phone")]
        [DataType(DataType.PhoneNumber)]
        [MaxLength(10, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"^(\+\d{1,3}[- ]?)?\d{10}$", ErrorMessage = "Invalid phone number format")]
        public string PhoneNumberTwo { get; set; }

        [Display(Name = "2nd Additional Phone")]
        [DataType(DataType.PhoneNumber)]
        [MaxLength(10, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"^(\+\d{1,3}[- ]?)?\d{10}$", ErrorMessage = "Invalid phone number format")]
        public string PhoneNumberThree { get; set; }
        [Required(ErrorMessage = "Phone number is required")]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone Number")]
        [MaxLength(10, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"^(\+\d{1,3}[- ]?)?\d{10}$", ErrorMessage = "Invalid phone number format")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Invalid email format")]
        //[Remote("IsEmailExists", "AddressBook", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string Email { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "1st Additional Email")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Invalid email format")]
        public string EmailTwo { get; set; }

        [DataType(DataType.EmailAddress)]
        [Display(Name = "2nd Additional Email")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Invalid email format")]
        public string EmailThree { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [Display(Name = "First Name")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name = "Last Name")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string LastName { get; set; }

        [Display(Name = "Institution Name")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string InstitutionName { get; set; }
        public string InstitutionDisplay { get; set; }


        [Display(Name = "Institution Description")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string InstitutionDescription { get; set; }
        public byte[] FileBytes { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public Nullable<bool> IsActive { get; set; }
    }
}
