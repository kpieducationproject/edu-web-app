﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.ViewModels
{
    public class ProfileViewModel
    {
        public string OldRoleId { get; set; }
        public string InstitutionDisplay { get; set; }
        public string SenderId { get; set; }
        [Display(Name = "User Role")]
        public string RoleId { get; set; }
        public IEnumerable<SelectListItem> UserRoles { get; set; }
        [Display(Name = "Full Name")]
        public string FullName
        {
            get { return FirstName + " " + LastName; }
            set { }
        }
        public string InstitutionReadOnly { get; set; }
        public string InstitutionDisabled { get; set; }

        public string FullNameEmailInstitutionName
        {
            get { return FirstName + " " + LastName + " - " + Email + $" (" + InstitutionName + ")"; }
            set { }
        }
        public string FullNameEmail
        {
            get { return FirstName + " " + LastName + " - " + Email; }
            set { }
        }

        [Required(ErrorMessage = "Email is required")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[A-Za-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Invalid email format")]
        [Remote("IsUpdateEditEmailExists", "Users", HttpMethod = "POST", ErrorMessage = "Email already exists", AdditionalFields = "UserId")]
        public string Email { get; set; }

        //[Required(ErrorMessage = "School is required")]
        [Display(Name = "School")]
        public int SchoolId { get; set; }
        public IEnumerable<SelectListItem> Schools { get; set; }
        public string Username { get; set; }

        //[Required(ErrorMessage = "Password is required")]
        [RegularExpression(@"^[a-zA-Z0-9\s]{6,}$", ErrorMessage = "Password length must be at least 6 characters")]
        [StringLength(255, ErrorMessage = "Must be between 6 and 255 characters", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        //[Required(ErrorMessage = "Confirm password is required")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The password and confirmation password do not match")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        //[Required(ErrorMessage = "New password is required")]
        [RegularExpression(@"^[a-zA-Z0-9\s]{6,}$", ErrorMessage = "New password length must be at least 6 characters")]
        [StringLength(255, ErrorMessage = "Must be between 6 and 255 characters", MinimumLength = 6)]
        [Remote("ContainsUsername", "Users", HttpMethod = "POST", ErrorMessage = "Password can’t contain your email or any part of your email", AdditionalFields = "Email")]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string ProfileNewPassword { get; set; }

        //[Required(ErrorMessage = "Confirm new password is required")]
        [System.ComponentModel.DataAnnotations.Compare("ProfileNewPassword", ErrorMessage = "New password and confirmation password do not match")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm New Password")]
        public string ProfileConfirmNewPassword { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [Display(Name = "Last Name")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string LastName { get; set; }
        [Display(Name = "Institution Name")]
        public string InstitutionName { get; internal set; }


        [Required(ErrorMessage = "Id Number is required")]
        [Display(Name = "Id Number")]
        [MinLength(1, ErrorMessage = "{0} cannot be less than {1} characters")]
        [RegularExpression(@"^(\d{13})?$", ErrorMessage = "Enter a 13 digit Id Number")]
        [MaxLength(13, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [Remote("IsUpdateEditIdNumExists", "Users", HttpMethod = "POST", ErrorMessage = "Id number already exists", AdditionalFields = "UserId")]
        public string IdNumber { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [Display(Name = "First Name")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Street Address is required")]
        [Display(Name = "Street Address")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string StreetAddress { get; set; }

        [Required(ErrorMessage = "Suburb is required")]
        [Display(Name = "Suburb")]
        [MaxLength(255, ErrorMessage = "{0} cannot be greater than {1} characters")]
        public string Suburb { get; set; }

        [Required(ErrorMessage = "Phone Number is required")]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [MaxLength(10, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"^(\+\d{1,3}[- ]?)?\d{10}$", ErrorMessage = "Invalid phone number format")]
        public string PhoneNumber { get; set; }

        [Display(Name = "Home Phone")]
        [DataType(DataType.PhoneNumber)]
        [MaxLength(10, ErrorMessage = "{0} cannot be greater than {1} characters")]
        [RegularExpression(@"^(\+\d{1,3}[- ]?)?\d{10}$", ErrorMessage = "Invalid phone number format")]
        public string HomePhone { get; set; }

        public string UserId { get; set; }

        public bool IsDeleted { get; set; }
        [Display(Name = "Role")]
        public string RoleName { get; internal set; }
    }
}