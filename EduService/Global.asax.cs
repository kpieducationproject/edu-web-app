﻿using EduService.Controllers;
using EduService.Services;
using System;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace EduService
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();

            ////Not Found (When user digit unexisting url)
            if (ex is HttpException && ((HttpException)ex).GetHttpCode() == 404)
            {
                HttpContextWrapper contextWrapper = new HttpContextWrapper(this.Context);

                RouteData routeData = new RouteData();
                routeData.Values.Add("controller", "Error");
                routeData.Values.Add("action", "Error404");

                IController controller = new ErrorController();
                RequestContext requestContext = new RequestContext(contextWrapper, routeData);
                controller.Execute(requestContext);
                Response.End();
            }
            //else //Unhandled Errors from aplication
            //{
            //    ErrorLogService.LogError(ex);
            //    HttpContextWrapper contextWrapper = new HttpContextWrapper(this.Context);

            //    RouteData routeData = new RouteData();
            //    routeData.Values.Add("controller", "Error");
            //    routeData.Values.Add("action", "Error500");

            //    IController controller = new ErrorController();
            //    RequestContext requestContext = new RequestContext(contextWrapper, routeData);
            //    controller.Execute(requestContext);
            //    Response.End();
            //}
        }
    }
}