﻿using EduService.BusinessLayer;
using EduService.Models;
using EduService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.Controllers
{
    [HandleError(View = "CustomErrorView")]
    [Authorize(Roles = "HOD")]
    public class SubjectsController : Controller
    {
        

        public EduServiceDBEntities db = new EduServiceDBEntities();
        public SubjectBL subjectsBL = new SubjectBL();

        //Subject Index
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            List<SubjectViewModel> subjectsList = new List<SubjectViewModel>();
            
            var subjects = subjectsBL.GetAllSubjectLists(subjectsList);

            return View(subjects);
        }

        //
        // Create 
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Institution/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SubjectViewModel subjectvm)
        {
            var result = string.Empty;
            if (ModelState.IsValid)
            {
                var checkIfExists = subjectsBL.IfSubjectExists(subjectvm.subjectName);

                if (checkIfExists == null)
                {
                    var Id = subjectsBL.Add(subjectvm);
                    if (Id > 0)
                    {
                        TempData["SubjectCreationSuccessful"] = subjectvm.subjectName + " has been added successfully!";
                        return RedirectToAction("Index", "Subjects");
                    }
                    else
                    {
                        TempData["SubjectCreationFailed"] = "Failed to create subject, please try again.";
                    }
                }
                else
                {
                    ModelState.AddModelError("SubjectName", "Subject already exists");
                }
            }

            return View(subjectvm);
        }

        [HttpPost]
        public JsonResult IsSubjectExists(string Name)
        {
            return Json(subjectsBL.IfSubjectExists(Name));
        }

        //Delete Subject
        public ActionResult Delete(int Id)
        {
            
            tbl_Subjects sub = db.tbl_Subjects.Find(Id);
            
            if (sub != null)
            {
                // Call the DeleteRecipient method to remove the recipient
                subjectsBL.Delete(Id);
                TempData["DeletedSubject"] = sub.subjectName + " was deleted successfully.";
            }
            else
            {
                //TempData["DeletedSubject"] = "Could not Delete " + sub.subjectName + ". Please Try Again.";
            }

           

            return RedirectToAction("Index", "Subjects");
        }

    }
}