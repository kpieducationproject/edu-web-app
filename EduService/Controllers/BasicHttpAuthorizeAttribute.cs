﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Security;

namespace EduService.Controllers
{
    internal class BasicHttpAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool IsAuthorized(HttpActionContext actionContext)
        {
            HttpContext httpContext = HttpContext.Current;

            bool IsCorrect = false;
            string authHeader = httpContext.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');
                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);
                if (username == "comArogornPortal" && password == "ArogornPortalcom")
                {
                    IsCorrect = true;
                }
            }
            return IsCorrect;
            //bool isAuthorized = false;
            //if (Thread.CurrentPrincipal.Identity.Name.Length == 0)
            //{ // If an identity has not already been established by other means:
            //    AuthenticationHeaderValue auth = actionContext.Request.Headers.Authorization;
            //    if (string.Compare(auth.Scheme, "Basic", StringComparison.OrdinalIgnoreCase) == 0)
            //    {
            //        string credentials = UTF8Encoding.UTF8.GetString(Convert.FromBase64String(auth.Parameter));
            //        int separatorIndex = credentials.IndexOf(':');
            //        if (separatorIndex >= 0)
            //        {
            //            string userName = credentials.Substring(0, separatorIndex);
            //            string password = credentials.Substring(separatorIndex + 1);
            //            if (userName == "comArogornPortal" && password == "ArogornPortalcom")
            //            {
            //                isAuthorized = true;
            //            }
            //        }
            //    }
            //}
            //return isAuthorized;
        }
        public bool VerifyCredentials()
        {
            HttpContext httpContext = HttpContext.Current;

            bool IsCorrect = false;
            string authHeader = httpContext.Request.Headers["Authorization"];
            if (authHeader != null && authHeader.StartsWith("Basic"))
            {
                //Extract credentials
                string encodedUsernamePassword = authHeader.Substring("Basic ".Length).Trim();
                System.Text.Encoding encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
                string usernamePassword = encoding.GetString(Convert.FromBase64String(encodedUsernamePassword));

                int seperatorIndex = usernamePassword.IndexOf(':');
                var username = usernamePassword.Substring(0, seperatorIndex);
                var password = usernamePassword.Substring(seperatorIndex + 1);
                if (username == "comArogornPortal" && password == "ArogornPortalcom")
                {
                    IsCorrect = true;
                }
            }
            return IsCorrect;
        }
    }

}