﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EduService.Models;

namespace EduService.Controllers
{

    public class IdentitiyController : Controller
    {

        public ApplicationRoleManager RoleManager()
        {
            var manager = new ApplicationRoleManager(new RoleStore<IdentityRole>(new ApplicationDbContext()));
            return manager;
        }
        public ApplicationUserManager UserManager()
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(new ApplicationDbContext()));
            return manager;
        }
    }
}
