﻿using EduService.BusinessLayer;
using EduService.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EduService.Controllers
{
    //[Authorize]

    public class MainController : Controller
    {
        //
        // GET: /Main/
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }
    }
}
