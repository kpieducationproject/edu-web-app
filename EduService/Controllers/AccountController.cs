﻿using EduService.BusinessLayer;
using EduService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EduService.Helpers;
using EduService.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using System.Diagnostics;
using CommonLibrary;
using System.Net.Mail;

namespace EduService.Controllers
{
    [HandleError(View = "CustomErrorView")]
    [Authorize]
    public class AccountController : Controller
    {
        //public  TimesheetDBEntities db = new TimesheetDBEntities();
        IdentitiyController identityController = new IdentitiyController();
        public Cryptography _cryptography = new Cryptography();
        public RoleBL roleBL = new RoleBL();
        public UsersBL userBL = new UsersBL();
        public InstitutionBL institutionBL = new InstitutionBL();
        LinkExpirationBL linkExpBL = new LinkExpirationBL();

        private ApplicationSignInManager _signInManager;

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        internal static ApplicationUserManager _userManager;


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Blank(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUsers user = userBL.FindUserById(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            return PartialView("Blank", user);
        }

        //
        // GET: /Account/

        public ActionResult Index()
        {

            return View();
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> Login(string returnUrl = "")
        {
            //if (Request.IsAuthenticated)
            //{
            //    return View();
            //}

            HttpCookie cookie = Request.Cookies["loginInfo"];

            if (cookie != null && cookie.HasKeys == true)
            {
                try
                {
                    var username = _cryptography.Decrypt(cookie.Values["Username"].ToString());
                    var password = _cryptography.Decrypt(cookie.Values["Password"].ToString());

                    var user = identityController.UserManager().FindByName(username);
                    if (user == null || (user != null && user.IsDeleted == true))
                    {
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View();
                    }
                    else if (user != null && !user.EmailConfirmed)
                    {
                        ModelState.AddModelError("", "Please verify your account before logging in.");
                        return View();
                    }
                    else
                    {
                        var result = await SignInManager.PasswordSignInAsync(username, password, true, shouldLockout: false);

                        switch (result)
                        {
                            case SignInStatus.Success:
                                return RedirectToLocal(returnUrl);
                            case SignInStatus.LockedOut:
                                return View("Lockout");
                            case SignInStatus.RequiresVerification:
                                return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = true });
                            case SignInStatus.Failure:
                            default:
                                return View();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                    return View();
                }
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel loginmodel, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(loginmodel);
            }
            else
            {
                var user = identityController.UserManager().FindByName(loginmodel.Username);
                if (user == null || (user != null && user.IsDeleted == true))
                {
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(loginmodel);
                }
                else if (user != null && !user.EmailConfirmed)
                {
                    ModelState.AddModelError("", "Please verify your account before logging in.");
                    return View(loginmodel);
                }
                else
                {
                    loginmodel.Username = loginmodel.Username.ToLower();

                    try
                    {

                        var result = await SignInManager.PasswordSignInAsync(loginmodel.Username, loginmodel.Password, loginmodel.RememberMe, shouldLockout: false);
                        switch (result)
                        {
                            case SignInStatus.Success:
                                if (loginmodel.RememberMe == true)
                                {
                                    //Create a cookie to save Login Infomation
                                    HttpCookie _rememberMeCookie = new HttpCookie("loginInfo");

                                    //Check if there's an existing cookie and if there is, then don't create one
                                    if (_rememberMeCookie.Expires < DateTime.Now)
                                    {
                                        _rememberMeCookie.Values.Add("Username", _cryptography.Encrypt(loginmodel.Username)); //UserName
                                        _rememberMeCookie.Values.Add("Password", _cryptography.Encrypt(loginmodel.Password)); //Password
                                        _rememberMeCookie.Expires = DateTime.Now.AddDays(30d); //Will expire in 30 days
                                        Response.Cookies.Add(_rememberMeCookie);
                                    }
                                }
                                else if (loginmodel.RememberMe == false)
                                {
                                    HttpCookie _rememberMeCookie = new HttpCookie("loginInfo");
                                    Response.Cookies.Add(_rememberMeCookie);
                                }
                                return RedirectToLocal(returnUrl);
                            case SignInStatus.LockedOut:
                                return View("Lockout");
                            case SignInStatus.RequiresVerification:
                                return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = loginmodel.RememberMe });
                            case SignInStatus.Failure:
                            default:
                                ModelState.AddModelError("", "Invalid login attempt.");
                                return View(loginmodel);
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.Write(ex.Message);

                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(loginmodel);
                    }
                }
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Dashboard");
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult IsUsernameExists(string email)
        {
            return Json(userBL.IsUsernameExists(email));
        }
        [HttpPost]
        [AllowAnonymous]
        public JsonResult IsIdNumExists(string IdNumber)
        {
            return Json(userBL.IsIdNumExists(IdNumber));
        }

        private IEnumerable<SelectListItem> PopulateRolesDropDownList()
        {
            CreateUserViewModel user = new CreateUserViewModel();

            var roles = identityController.RoleManager().Roles.ToList();

            if (roles.Count > 0)
            {
                user.UserRoles = new SelectList(roles.ToList(), "Id", "Name");
            }
            else
            {
                user.UserRoles = Enumerable.Empty<SelectListItem>();
            }

            return user.UserRoles;
        }
        private IEnumerable<SelectListItem> PopulateCompaniesDropDownList()
        {
            CreateUserViewModel user = new CreateUserViewModel();
            var companies = institutionBL.GetAllSchools();

            if (companies.Count > 0)
            {
                user.Companies = new SelectList(companies.ToList(), "Id", "Name");
            }
            else
            {
                user.Companies = Enumerable.Empty<SelectListItem>();
            }

            return user.Companies;
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Activate()
        {
            LogOffWithoutSessionAbandon();

            ResetPasswordViewModel vm = new ResetPasswordViewModel();

            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Activate(ResetPasswordViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            else
            {


                try
                {
                    var email = vm.Email;
                    var user = UserManager.FindByEmail(vm.Email);
                    if (user == null)
                    {
                        ViewBag.message = "Failed";
                    }
                    else if (!user.EmailConfirmed)
                    {
                        ViewBag.message = "Activated";
                    }
                    else
                    {
                        if (user != null && user.IsDeleted == false)
                        {
                            string originalPath = Request.Url.ToString();
                            string baseUrl = originalPath.Substring(0, originalPath.LastIndexOf("/Account"));
                            string encrytedEmail = _cryptography.Encrypt(vm.Email);

                            string subject = $"E-Learning - Activate Account";
                            string emailMsg = string.Empty;
                            emailMsg += "Hi";
                            emailMsg += $"<br/><br/>Account activation request was made for {vm.Email}. If you didn't, please ignore email.";
                            emailMsg += "<br/><br/>";
                            emailMsg += "Click <a href ='" + baseUrl + "/Account/SetPAassword?" + "id=" + encrytedEmail + "'>here</a> to reset password.";

                            EmailBL emailbl = new EmailBL();
                            MailMessage msg = new MailMessage($"no-reply@coralite.co.za", vm.Email, subject, emailMsg);
                            msg.IsBodyHtml = true;
                            emailbl.SendEmail().Send(msg);

                            TempData["ActivationEmail"] = "Activation link sent successfully. Please check your email.";

                            tbl_LinkExpiration linkExp = new tbl_LinkExpiration();
                            linkExp.UserId = user.Id;
                            linkExpBL.Add(linkExp);

                            return LogOffWithoutSessionAbandon();
                        }
                        else
                        {
                            ViewBag.message = "Failed";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                    ViewBag.message = "Failed";
                }
            }
            return View(vm);
        }


        //private IEnumerable<SelectListItem> PopulateRolesDropDownList()
        //{
        //    CreateUserViewModel user = new CreateUserViewModel();

        //    var roles = identityController.RoleManager().Roles.ToList();

        //    if (roles.Count > 0)
        //    {
        //        user.UserRoles = new SelectList(roles.ToList(), "Id", "Name");
        //    }
        //    else
        //    {
        //        user.UserRoles = Enumerable.Empty<SelectListItem>();
        //    }

        //    return user.UserRoles;
        //}

        public void createRoles()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            // creating Creating Manager role     
            if (!roleManager.RoleExists("System Admin"))
            {
                var role = new IdentityRole();
                role.Name = "System Admin";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Teacher"))
            {
                var role = new IdentityRole();
                role.Name = "Teacher";
                roleManager.Create(role);
            }

            if (!roleManager.RoleExists("Learner"))
            {
                var role = new IdentityRole();
                role.Name = "Learner";
                roleManager.Create(role);
            }

        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();

            HttpCookie _rememberMeCookie = new HttpCookie("loginInfo");
            Response.Cookies.Add(_rememberMeCookie);
            return RedirectToAction("Login", "Account");
        }
        [AllowAnonymous]
        public ActionResult LogOffWithoutSessionAbandon()
        {
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpCookie _rememberMeCookie = new HttpCookie("loginInfo");
            Response.Cookies.Add(_rememberMeCookie);
            return RedirectToAction("Login", "Account");
        }
        public ActionResult SetSession()
        {
            Session["Test"] = "Test Value";
            return View();
        }

        public ActionResult Keepalive()
        {
            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword()
        {
            LogOffWithoutSessionAbandon();

            ResetPasswordViewModel vm = new ResetPasswordViewModel();

            return View(vm);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ResetPassword(ResetPasswordViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            else
            {
                try
                {
                    var email = vm.Email;
                    var user = UserManager.FindByEmail(vm.Email);
                    if (user == null)
                    {
                        ViewBag.message = "Failed";
                    }
                    else if (!user.EmailConfirmed)
                    {
                        ViewBag.message = "Not Activated";
                    }
                    else
                    {
                        if (user != null && user.IsDeleted == false)
                        {
                            string originalPath = Request.Url.ToString();
                            string baseUrl = originalPath.Substring(0, originalPath.LastIndexOf("/Account"));
                            string encrytedEmail = _cryptography.Encrypt(vm.Email);

                            string subject = $"E-Learning Forgot Password";
                            string emailMsg = string.Empty;
                            emailMsg += "Hi";
                            emailMsg += $"<br/><br/>Reset password request was made for {vm.Email}. If you didn't, please ignore email.";
                            emailMsg += "<br/><br/>";
                            emailMsg += "Click <a href ='" + baseUrl + "/Account/SetPassword?" + "id=" + encrytedEmail + "'>here</a> to reset password.";

                            EmailBL emailbl = new EmailBL();
                            MailMessage msg = new MailMessage($"no-reply@coralite.co.za", vm.Email, subject, emailMsg);
                            msg.IsBodyHtml = true;
                            emailbl.SendEmail().Send(msg);

                            TempData["ResetEmail"] = $"A link to reset your password was sent successfully sent to {vm.Email}. Please check your email.";


                            tbl_LinkExpiration linkExp = new tbl_LinkExpiration();
                            linkExp.UserId = user.Id;
                            linkExpBL.Add(linkExp);

                            return LogOffWithoutSessionAbandon();
                        }
                        else
                        {
                            ViewBag.message = "Failed";
                        }
                    }
                }
                catch (Exception ex)
                {
                    Debug.Write(ex.Message);
                    ViewBag.message = "Failed";
                }
            }
            return View(vm);
        }
        [AllowAnonymous]
        public ActionResult SetPassword(string id)
        {
            SetPasswordViewModel vm = new SetPasswordViewModel();
            try
            {
                var querystringParameter = _cryptography.Decrypt(id);
                var email = querystringParameter;
                if (!string.IsNullOrWhiteSpace(email))
                {
                    var aspUser = userBL.FindUserByEmail(email);
                    if (aspUser != null && aspUser.IsDeleted == false)
                    {
                        var linkExpiration = linkExpBL.GetActivationLink(aspUser.Id);
                        if (linkExpiration != null)
                        {
                            if (DateTime.Now > linkExpiration.DateCreated.AddHours(8))
                            {
                                ViewBag.message = "Link Expired";
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(aspUser.Email))
                                {
                                    vm.Email = aspUser.Email;
                                }
                            }
                        }
                        else
                        {
                            ViewBag.message = "Link not found, please request a new link";
                        }
                    }
                    else
                    {
                        ViewBag.message = "Unable to find user";
                    }
                }
                else
                {
                    ViewBag.message = "Unable to verify link, please request a new link";
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
                ViewBag.message = "Unable to verify link, please request a new link";
            }
            return View(vm);
        }
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult SetPassword(SetPasswordViewModel vm)
        {
            if (!ModelState.IsValid)
            {
                return View(vm);
            }
            else
            {
                var aspnetUser = identityController.UserManager().FindByName(vm.Email);
                var linkExpiration = linkExpBL.GetActivationLink(aspnetUser.Id);

                if (aspnetUser != null && linkExpiration != null && linkExpiration.Id > 0)
                {
                    vm.Password = UserManager.PasswordHasher.HashPassword(vm.Password);

                    var updated = userBL.VerifyUser(vm);
                    if (updated)
                    {
                        string originalPath = Request.Url.ToString();
                        string baseUrl = originalPath.Substring(0, originalPath.LastIndexOf("/Account"));
                        string encrytedEmail = _cryptography.Encrypt(vm.Email);

                        string subject = $"E-Learning - Password Updated";
                        string emailMsg = string.Empty;
                        emailMsg += "Hi";
                        emailMsg += $"<br/><br/>Password for {vm.Email} was successfully updated.";

                        EmailBL emailbl = new EmailBL();
                        MailMessage msg = new MailMessage($"no-reply@coralite.co.za", vm.Email, subject, emailMsg);
                        msg.IsBodyHtml = true;
                        emailbl.SendEmail().Send(msg);

                        TempData["PasswordSaved"] = "Your password was saved successfully.";

                        linkExpBL.Delete(linkExpiration.Id);

                        return LogOffWithoutSessionAbandon();
                    }
                    else
                    {

                        ModelState.AddModelError("", "Unable to set password.");

                    }
                }
                else
                {
                    ModelState.AddModelError("", "Unable to set password.");
                }
            }
            return View(vm);
        }
    }
}