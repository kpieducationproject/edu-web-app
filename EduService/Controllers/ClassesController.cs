﻿using EduService.BusinessLayer;
using EduService.Models;
using EduService.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EduService.Controllers
{
    public class ClassesController : Controller
    {
        internal static ApplicationUserManager _userManager;
        internal static ApplicationRoleManager _appRoleManager;

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _appRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _appRoleManager = value;
            }
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        ClassesBL classesBL = new ClassesBL();
        GradesBL gradesBL = new GradesBL();
        UsersBL userBL = new UsersBL();
        SubjectBL subjectsBL = new SubjectBL();
        // GET: Classes
        public ActionResult Index()
        {

            string userId = User.Identity.GetUserId();
            var aspuser = userBL.FindUserById(userId);
            List<ClassesViewModel> list = new List<ClassesViewModel>();

            if (aspuser != null)
            {
                var vm = classesBL.GetClasses(aspuser.InstitutionId);
                return View(vm);
            }
            else
            {
                return View(list);
            }
        }

        // GET: /Users/CreateUser
        [Authorize(Roles = "HOD, Secretary")]
        [HttpGet]
        public async Task<ActionResult> Create()
        {
            ClassesViewModel classesVM = new ClassesViewModel();
            //createRoles();
            classesVM.Divisions = PopulateDivisionsDropDownList();
            classesVM.Grades = PopulateGradesDropDownList();
 
            classesVM.Learners = await PopulateLearnersDropDownList();
            classesVM.Teachers = await PopulateTeachersDropDownList();

            return View(classesVM);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ClassesViewModel classes)
        {
            var result = string.Empty;
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();
                var aspuser = userBL.FindUserById(userId);
                if (aspuser != null)
                {
                    classes.SchoolId = aspuser.InstitutionId;
                }
                //check if user does not exist
                if (!classesBL.IsExists(classes.Name, classes.GradeId, classes.SchoolId))
                {
                    var claseCreationResult = classesBL.Add(classes);

                    if (claseCreationResult > 0)
                    {

                        TempData["createdClassMessage"] = classes.Name + " has been added successfully";

                        return RedirectToAction("Index", "Classes");
                    }
                    else
                    {
                        ModelState.AddModelError("", "Unable to create class, please try again");
                    }
                }
                else
                {
                    TempData["createClassErrorMessage"] = classes.Name + "for specified grade and subject already exists, please try again.";
                }
            }

            classes.Learners = await PopulateLearnersDropDownList();
            classes.Grades = PopulateGradesDropDownList();
            classes.Teachers = await PopulateTeachersDropDownList();

            classes.Divisions = PopulateDivisionsDropDownList();
            return View(classes);
        }

        private IEnumerable<SelectListItem> PopulateGradesDropDownList()
        {
            ClassesViewModel classes = new ClassesViewModel();

            var grades = gradesBL.GetGrades();

            if (grades.Count > 0)
            {
                classes.Grades = new SelectList(grades.ToList(), "Id", "Grade");
            }
            else
            {
                classes.Grades = Enumerable.Empty<SelectListItem>();
            }

            return classes.Grades;
        }

        [HttpGet]
        public ActionResult GetLearnersForSubjectGrade(int gradeId)
        {
            List<string> learnersList = new List<string>();
            string userId = User.Identity.GetUserId();
            var aspuser = userBL.FindUserById(userId);
            if (aspuser != null)
            {
                var role = RoleManager.FindByName("Learner");
                var aspusers = new List<ApplicationUser>();
                foreach (var user in UserManager.Users.ToList())
                {
                    if (UserManager.IsInRole(user.Id, role.Name))
                    {
                        aspusers.Add(user);
                    }
                }
                var learners = classesBL.GetLearnersForClass(gradeId, aspuser.InstitutionId, aspusers);
                if (learners != null && learners.Count > 0)
                {
                    foreach (var learner in learners)
                    {
                        learnersList.Add("<option value='" + learner.Id + "'>" + learner.FirstName + " " + learner.LastName + "</option>");
                    }
                }

            }

            return Content(String.Join("", learnersList));
        }
        private async Task<IEnumerable<SelectListItem>> PopulateLearnersDropDownList()
        {
            ClassesViewModel vm = new ClassesViewModel();
            string userId = User.Identity.GetUserId();
            var aspuser = userBL.FindUserById(userId);
            if (aspuser != null)
            {
                var role = RoleManager.FindByName("Learner");
                var aspusers = new List<ApplicationUser>();
                foreach (var user in UserManager.Users.ToList())
                {
                    if (await UserManager.IsInRoleAsync(user.Id, role.Name))
                    {
                        aspusers.Add(user);
                    }
                }

                if (aspusers.Count > 0)
                {
                    vm.Learners = new SelectList(aspusers, "Id", "FirstName");
                }
                else
                {
                    vm.Learners = Enumerable.Empty<SelectListItem>();
                }
            }


            return vm.Learners;
        }
        private async Task<IEnumerable<SelectListItem>> PopulateTeachersDropDownList()
        {
            ClassesViewModel vm = new ClassesViewModel();
            string userId = User.Identity.GetUserId();
            var aspuser = userBL.FindUserById(userId);
            if (aspuser != null)
            {
                var role = RoleManager.FindByName("Teacher");
                var aspusers = new List<ApplicationUser>();
                foreach (var user in UserManager.Users.ToList())
                {
                    if (await UserManager.IsInRoleAsync(user.Id, role.Name))
                    {
                        aspusers.Add(user);
                    }
                }

                if (aspusers.Count > 0)
                {
                    vm.Teachers = new SelectList(aspusers, "Id", "FirstName");
                }
                else
                {
                    vm.Teachers = Enumerable.Empty<SelectListItem>();
                }
            }


            return vm.Teachers;
        }
        DivisionBL divisionBL = new DivisionBL();
        private IEnumerable<SelectListItem> PopulateDivisionsDropDownList()
        {
            ClassesViewModel vm = new ClassesViewModel();
            var items = divisionBL.GetAllDivisions();

            if (items.Count > 0)
            {
                vm.Divisions = new SelectList(items.ToList(), "Id", "divisionDescription");
            }
            else
            {
                vm.Divisions = Enumerable.Empty<SelectListItem>();
            }

            return vm.Divisions;
        }
         public ActionResult Delete(int Id)
        {
            var deleted = classesBL.Delete(Id);

            if (deleted)
            {
                return RedirectToAction("Index", "Classes");
            }

            return View();
        }
    }
}