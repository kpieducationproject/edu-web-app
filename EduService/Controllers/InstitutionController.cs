﻿using EduService.BusinessLayer;
using EduService.Models;
using EduService.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.Controllers
{
    [HandleError(View = "CustomErrorView")]
    [Authorize(Roles = "System Admin")]
    public class InstitutionController : Controller
    {
        public EduServiceDBEntities db = new EduServiceDBEntities();
        public InstitutionBL institutionBL = new InstitutionBL();

        // GET: Institution
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Account");

            List<InstitutionViewModel> institutionList = new List<InstitutionViewModel>();

            var institutions = institutionBL.GetAllCompanies(institutionList);

            return View(institutions);
        }

        // GET: /Institution/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Institution/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InstitutionViewModel institutionvm)
        {
            var result = string.Empty;
            if (ModelState.IsValid)
            {
                var checkIfExists = institutionBL.IfInstitutionExists(institutionvm.Name);

                if (checkIfExists == null)
                {
                    var Id = institutionBL.Add(institutionvm);
                    if (Id > 0)
                    {
                        TempData["InstitutionCreationSuccessful"] = institutionvm.Name + " has been added successfully!";
                        return RedirectToAction("Index", "Institution");
                    }
                    else
                    {
                        TempData["InstitutionCreationFailed"] = "Failed to create institution, please try again.";
                    }
                }
                else
                {
                    ModelState.AddModelError("InstitutionName", "Institution already exists");
                }
            }

            return View(institutionvm);
        }

        [HttpPost]
        public JsonResult IsInstitutionExists(string Name)
        {
            return Json(institutionBL.CheckIfInstitutionExists(Name));
        }

        //
        //Institution/Delete/
        public ActionResult Delete(int Id)
        {
            tbl_Institution ins = db.tbl_Institution.Find(Id); // Find the person specified

            // Check if the person is a recipient

            // Check if the person exist as a recipient
            if (ins != null)
            {
                // Call the DeleteRecipient method to remove the recipient
                institutionBL.Delete(Id);
            }

            TempData["DeletedInstitution"] = ins.Name + " was deleted successfully.";

            return RedirectToAction("Index", "Institution");
        }
    }
}