﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using EduService.BusinessLayer;
using EduService.Models;
using EduService.ViewModels;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using CommonLibrary;
using System.Net.Mail;
namespace EduService.Controllers
{
    [Authorize(Roles = "HOD, Teacher, System Admin, Learner, School Admin")]
    [HandleError(View = "CustomErrorView")]
    public class UsersController : Controller
    {
        internal static ApplicationUserManager _userManager;
        internal static ApplicationSignInManager _signInManager;
        internal static ApplicationRoleManager _appRoleManager;
         public Cryptography _cryptography = new Cryptography();
        LinkExpirationBL linkExpBL = new LinkExpirationBL();

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _appRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _appRoleManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public EduServiceDBEntities db = new EduServiceDBEntities();
        public UsersBL userBl = new UsersBL();
        public LearnerGradeBL learnerGradeBL = new LearnerGradeBL();
        public GradesBL gradesBL = new GradesBL();
        RoleBL roleBL = new RoleBL();
        InstitutionBL institutionBL = new InstitutionBL();
        UsersBL userBL = new UsersBL();
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            var loggedInUser = userBL.GetUser(userId);

            var userRole = GetUserRoleByUserId(userId);

            if (loggedInUser != null)
            {
                var users = userBL.GetUsers(userRole, loggedInUser.InstitutionId);

                var currentUser = users.FirstOrDefault(x => x.UserId == loggedInUser.Id);
                users.Remove(currentUser);

                foreach (var item in users.ToList())
                {
                    item.RoleName = GetUserRoleByUserId(item.UserId);
                }
                return View(users);
            }
            else
            {
                return View();
            }
        }

        [HttpGet]
        public string GetUserRoleByUserId(string IdNumber)
        {
            string role = "Unknown Role";

            var userExists = userBL.FindUserById(IdNumber);

            if (userExists != null)
            {
                var roles = UserManager.GetRoles(IdNumber);

                if (roles != null)
                {
                    role = roles.FirstOrDefault();
                }
            }

            return role;
        }
        //
        // GET: /Users/CreateUser
        [Authorize(Roles = "Institution Admin, System Admin, School Admin")]
        [HttpGet]
        public ActionResult Create()
        {
            UserViewModel user = new UserViewModel();
            //createRoles();
            user.UserRoles = PopulateRolesDropDownList();
            user.Grades = PopulateGradesDropDownList();

            user.Schools = PopulateCompaniesDropDownList();

            if (user.Schools != null && user.Schools.ToList().Count > 0)
            {
                string userId = User.Identity.GetUserId();
                var aspuser = userBL.FindUserById(userId);
                if (aspuser != null)
                {
                    user.SchoolId = aspuser.InstitutionId;
                }
            }
            return View(user);
        }
        public IEnumerable<string> AllPartsOfLength(string value, int length)
        {
            for (int startPos = 0; startPos <= value.Length - length; startPos++)
            {
                yield return value.Substring(startPos, length);
            }
            yield break;
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult ContainsUsername(string password, string email)
        {
            bool result = true;


            bool usernameOk = !AllPartsOfLength(email.ToLower(), 3)
                .Any(part => password.ToLower().Contains(part));

            if (!usernameOk)
            {
                result = false;
            }

            return Json(result);
        }
        // POST: /Users/CreateUser
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(UserViewModel user)
        {
            var result = string.Empty;
            var role = RoleManager.FindById(user.RoleId);
            if (role.Name != "Learner")
            {
                if (ModelState.ContainsKey("GradeId"))
                    ModelState["GradeId"].Errors.Clear();
            }

            if (ModelState.IsValid)
            {

                if (user.SchoolId > 0)
                {
                    //check if user does not exist
                    if (!userBl.UserExists(user.IdNumber))
                    {
                        ApplicationUser aspuser = new ApplicationUser();

                        aspuser = new ApplicationUser { UserName = user.Username, Email = user.Username, FirstName = user.FirstName, LastName = user.LastName, IsDeleted = false, Suburb = user.Suburb, StreetAddress = user.StreetAddress, IdNumber = user.IdNumber, PhoneNumber = user.PhoneNumber };
                        if (!string.IsNullOrWhiteSpace(user.HomePhone))
                        {
                            aspuser.HomePhone = user.HomePhone;
                        }
                        if (user.SchoolId > 0)
                        {
                            aspuser.InstitutionId = user.SchoolId;
                        }
                        var userCreationResult = await UserManager.CreateAsync(aspuser);

                        if (userCreationResult.Succeeded)
                        {
                            var roleresult = UserManager.AddToRole(aspuser.Id, role.Name);
                            if (role.Name == "Learner" && user.GradeId > 0)
                            {
                                tbl_LearnerGrade learnerGrade = new tbl_LearnerGrade();
                                learnerGrade.GradeId = user.GradeId;
                                learnerGrade.UserId = aspuser.Id;
                                learnerGradeBL.Add(learnerGrade);
                            }
                            try
                            {
                                string originalPath = Request.Url.ToString();
                                string passString = _cryptography.Encrypt(aspuser.Email.ToString());
                                var school = institutionBL.GetInstitution(user.SchoolId);
                                string baseUrl = originalPath.Substring(0, originalPath.LastIndexOf("/User"));

                                string subject = $"E-Learning {role.Name} Account creation";
                                string emailMsg = string.Empty;
                                emailMsg += $"Your account has been successfully created on E-Learning as a {role.Name} to the below mentioned school:<br/> {school.Name}.";
                                emailMsg += "<br/><br/>";
                                emailMsg += "Click <a href ='" + baseUrl + "/Account/SetPassword/id=" + passString + "'>here</a> to set your password and activate your account.";

                                EmailBL emailbl = new EmailBL();
                                MailMessage msg = new MailMessage($"ashly.raju@coralite.co.za", aspuser.Email, subject, emailMsg);
                                msg.IsBodyHtml = true;
                                emailbl.SendEmail().Send(msg);
                                tbl_LinkExpiration linkExp = new tbl_LinkExpiration();
                                linkExp.UserId = aspuser.Id;
                                linkExpBL.Add(linkExp);
                                TempData["createdUserMessage"] = user.FirstName + " " + user.LastName + " has been added successfully. Activation link sent to your email address.";
                            }
                            catch (Exception ms3)
                            {
                                Debug.Write(ms3.Message);
                            }

                            return RedirectToAction("Index", "Users");
                        }
                        else
                        {
                            ModelState.AddModelError("", "Unable to create user, please try again");
                        }
                    }
                    //check if user exists but is deleted
                    else if (userBl.DeletedUserExists(user.IdNumber))
                    {
                        var toUpdate = UserManager.FindByEmail(user.Username);
                        if (toUpdate != null && !string.IsNullOrWhiteSpace(toUpdate.Id))
                        {
                            var rolesForUser = await UserManager.GetRolesAsync(toUpdate.Id);

                            if (rolesForUser.Count() > 0)
                            {
                                foreach (var item in rolesForUser.ToList())
                                {
                                    var end = await UserManager.RemoveFromRoleAsync(toUpdate.Id, item);
                                }
                            }

                            try
                            {
                                var roleresult = UserManager.AddToRole(toUpdate.Id, role.Name);
                                user.UserId = toUpdate.Id;
                                user.Password = UserManager.PasswordHasher.HashPassword(user.Password);
                                var updated = userBl.Update(user);
                                if (updated)
                                {
                                    TempData["createdUserMessage"] = user.FirstName + " " + user.LastName + " has been added successfully";
                                    return RedirectToAction("Index", "Users");
                                }
                                else
                                {
                                    ModelState.AddModelError("", "Unable to create user, please try again");
                                }
                            }
                            catch (Exception ex)
                            {
                                Debug.Write(ex.Message);

                            }
                        }
                    }
                    //check if user exists
                    else if (userBl.UserExists(user.IdNumber))
                    {
                        ModelState.AddModelError("IdNumber", "Id number already exists");
                    }
                }
                else if (user.GradeId == 0)
                {
                    ModelState.AddModelError("GradeId", "Please select a grade to add new user to");
                }
                else if (user.SchoolId == 0)
                {
                    ModelState.AddModelError("SchoolId", "Please select a institution to add new user to");
                }
            }
            user.Grades = PopulateGradesDropDownList();

            user.UserRoles = PopulateRolesDropDownList();
            user.Schools = PopulateCompaniesDropDownList();
            return View(user);
        }

        private IEnumerable<SelectListItem> PopulateCompaniesDropDownList()
        {
            CreateUserViewModel user = new CreateUserViewModel();
            var companies = institutionBL.GetAllSchools();

            if (companies.Count > 0)
            {
                user.Companies = new SelectList(companies.ToList(), "Id", "Name");
            }
            else
            {
                user.Companies = Enumerable.Empty<SelectListItem>();
            }

            return user.Companies;
        }
        [HttpPost]
        public JsonResult IsUsernameExists(string username)
        {
            return Json(userBL.IsUsernameExists(username));
        }
        [HttpPost]
        public JsonResult IsIdNumExists(string IdNumber)
        {
            return Json(userBL.IsIdNumExists(IdNumber));
        }
        [HttpPost]
        public JsonResult IsUpdateEditIdNumExists(string IdNumber, string userId)
        {
            return Json(userBL.IsNewIdExists(IdNumber, userId));
        }
        [HttpPost]
        public JsonResult IsUpdateEditEmailExists(string email, string userId)
        {
            return Json(userBL.IsUpdateEmailExists(email, userId));
        }
        [HttpGet]
        public string GetUserRole()
        {
            string userId = User.Identity.GetUserId();

            var roles = UserManager.GetRoles(userId);

            string role = "Unknown Role";

            if (roles != null)
            {
                role = roles.FirstOrDefault();
            }

            return role;
        }

        public string GetRoleId(string userId)
        {
            string roleId = "";

            var roleName = GetUserRoleByUserId(userId);
            if (!string.IsNullOrWhiteSpace(roleName))
            {
                var role = RoleManager.FindByName(roleName);
                //var roles = roleBL.GetRoles(userId);
                if (role != null)
                {
                    roleId = role.Id;
                }
            }

            return roleId;
        }

        [HttpGet]
        public ActionResult UpdateProfile()
        {
            var Id = User.Identity.GetUserId();
            ProfileViewModel user = new ProfileViewModel();
            user.UserRoles = PopulateRolesDropDownList();

            var aspuser = userBL.FindUserById(Id);
            if (aspuser != null)
            {
                user.UserId = Id;
                user.Email = aspuser.Email;
                user.Username = aspuser.UserName;
                user.FirstName = aspuser.FirstName;
                user.LastName = aspuser.LastName;
                user.SchoolId = aspuser.InstitutionId;
                user.IdNumber = aspuser.IdNumber;
                user.StreetAddress = aspuser.StreetAddress;
                user.Suburb = aspuser.Suburb;
                user.PhoneNumber = aspuser.PhoneNumber;
                if (!string.IsNullOrWhiteSpace(aspuser.HomePhone))
                {
                    user.HomePhone = aspuser.HomePhone;
                }
            }

            return View(user);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(ProfileViewModel user)
        {

            if (ModelState.IsValid)
            {

                if (!string.IsNullOrWhiteSpace(user.ProfileNewPassword))
                {
                    user.Password = UserManager.PasswordHasher.HashPassword(user.ProfileNewPassword);
                }
                bool updated = userBl.UpdateProfile(user);

                if (updated)
                {
                    TempData["updatedProfile"] = "Your profile has been updated successfully";
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    ModelState.AddModelError("", "Failed to update your profile. Please try again.");
                }
            }

            user.UserRoles = PopulateRolesDropDownList();
            user.Schools = PopulateCompaniesDropDownList();

            return View(user);

        }

        [Authorize(Roles = "Institution Admin, System Admin")]
        [HttpGet]
        [EncryptedActionParameter]
        public ActionResult Edit(string Id)
        {
            var aspuser = userBL.FindUserById(Id);
            EditUserViewModel user = new EditUserViewModel();
            user.UserRoles = PopulateRolesDropDownList();
            user.Schools = PopulateCompaniesDropDownList();
            if (aspuser != null)
            {
                if (user.Schools != null && user.Schools.ToList().Count > 0)
                {
                    var roles = UserManager.GetRoles(Id);

                    user.UserId = Id;
                    user.SchoolId = aspuser.InstitutionId;
                    user.Email = aspuser.Email;
                    user.Username = aspuser.UserName;
                    user.FirstName = aspuser.FirstName;
                    user.LastName = aspuser.LastName;
                    user.IdNumber = aspuser.IdNumber;
                    user.StreetAddress = aspuser.StreetAddress;
                    user.Suburb = aspuser.Suburb;
                    user.PhoneNumber = aspuser.PhoneNumber;
                    if (!string.IsNullOrWhiteSpace(aspuser.HomePhone))
                    {
                        user.HomePhone = aspuser.HomePhone;
                    }
                    if (roles != null && roles.Count > 0)
                    {
                        string roleId = roleBL.GetRoleIdByName(roles.FirstOrDefault());
                        if (!string.IsNullOrWhiteSpace(roleId))
                        {
                            user.RoleId = roleId;
                            user.OldRoleId = roleId;
                        }
                    }
                    else
                    {
                        var roleAddresult = UserManager.AddToRole(Id, "Institution User");
                        string roleId = roleBL.GetRoleIdByName("Institution User");
                        user.RoleId = roleId;
                        user.OldRoleId = roleId;
                    }
                    //user.SignatureImageByteString = GetSignatureImage(Id);
                }
            }
            else
            {
                return RedirectToAction("Index", "Users");

            }

            return View(user);
        }

        [Authorize(Roles = "HOD, Principal, System Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditUserViewModel user)
        {
            if (ModelState.IsValid)
            {

                if (!string.IsNullOrWhiteSpace(user.RoleId) && !string.IsNullOrWhiteSpace(user.OldRoleId) && user.RoleId != user.OldRoleId)
                {
                    var roleToRemove = RoleManager.FindById(user.OldRoleId);
                    var roleRemoveresult = UserManager.RemoveFromRole(user.UserId, roleToRemove.Name);
                    var roleToAdd = RoleManager.FindById(user.RoleId);
                    var roleAddresult = UserManager.AddToRole(user.UserId, roleToAdd.Name);
                }

                bool updated = userBl.EditUser(user);

                if (updated)
                {
                    TempData["updatedUser"] = user.FirstName + " " + user.LastName + " has been updated successfully";
                    return RedirectToAction("Index", "Users");
                }
                else
                {
                    ModelState.AddModelError("", "Failed to update user. Please try again.");
                }

            }

            user.UserRoles = PopulateRolesDropDownList();
            user.Schools = PopulateCompaniesDropDownList();

            return View(user);

        }

        [Authorize(Roles = "Institution Admin, System Admin")]
        public ActionResult Delete(string Id)
        {
            AspNetUsers user = db.AspNetUsers.Find(Id);

            if (user != null)
            {
                userBl.Delete(Id);
            }

            return RedirectToAction("Index", "Users");
        }
        private IEnumerable<SelectListItem> PopulateGradesDropDownList()
        {
            UserViewModel user = new UserViewModel();

            var grades = gradesBL.GetGrades();

            if (grades.Count > 0)
            {
                user.Grades = new SelectList(grades.ToList(), "Id", "Grade");
            }
            else
            {
                user.Grades = Enumerable.Empty<SelectListItem>();
            }

            return user.Grades;
        }
        private IEnumerable<SelectListItem> PopulateRolesDropDownList()
        {
            UserViewModel user = new UserViewModel();

            var roles = RoleManager.Roles.ToList();

            if (roles.Count > 0)
            {
                var systemadmin = roles.FirstOrDefault(x => x.Name == "System Admin");
                if (systemadmin != null)
                {
                    roles.Remove(systemadmin);
                }
                user.UserRoles = new SelectList(roles.ToList(), "Id", "Name");
            }
            else
            {
                user.UserRoles = Enumerable.Empty<SelectListItem>();
            }

            return user.UserRoles;
        }

        // GET: /User/GetUserName
        [HttpGet]
        public string GetUserName()
        {
            string userId = User.Identity.GetUserId();

            string username = "";

            var user = userBl.FindUserById(userId);
            if (user != null)
            {
                username = user.FirstName + " " + user.LastName;
            }
            else
            {
                username = "Unknown User";
            }

            return username;
        }

        // GET: /User/GetRoles
        [HttpGet]
        public string GetRoles()
        {
            string userId = User.Identity.GetUserId();

            string roleName = "";

            var roles = roleBL.GetRoles(userId);
            if (roles != null && roles.Count > 0)
            {
                if (roles.Count == 1)
                {
                    roleName = roles[0].Name;
                }
                else
                {
                    roleName = String.Join(", ", roles.Select(x => x.Name));
                }
            }
            else
            {
                roleName = "Unknown Role";
            }

            return roleName;
        }
    }
}
