﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using EduService.BusinessLayer;
using EduService.Models;
using EduService.ViewModels;

namespace EduService.Controllers
{

    [Authorize]
    [HandleError(View = "CustomErrorView")]
    public class DashboardController : Controller
    {
        IdentitiyController identityController = new IdentitiyController();
        public UsersBL userBL = new UsersBL();
        public InstitutionBL institutionBL = new InstitutionBL();

        [HttpGet]
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            if (GetUserRole() == null)
            {
                var roleresult = identityController.UserManager().AddToRole(userId, "Learner");
            }
            return View();
        }
        public string GetProfilePic()
        {
            string userId = User.Identity.GetUserId();

            string src = "";

            var user = userBL.FindUserById(userId);

            if (user != null)
            {
                var firstLetter = CommonCode.FirstLetterToUpper(user.FirstName.Trim());
                src = "profilepic/" + firstLetter + ".png";
            }
            else
            {
                src = "profile.png";
            }

            return src;
        }


        public string GetUserName()
        {
            string userId = User.Identity.GetUserId();
            string username = "";
            //var a = userBL.FindUserById(userId);

            var user = userBL.FindUserById(userId);
            if (user != null)
            {
                username = user.FirstName + " " + user.LastName;
            }
            else
            {
                username = "Unknown User";
            }

            return username;
        }

        public string GetUserRole()
        {
            string userId = User.Identity.GetUserId();

            var roles = identityController.UserManager().GetRoles(userId);

            string role = "Unknown Role";

            if (roles != null)
            {
                role = roles.FirstOrDefault();
            }

            return role;
        }
        public string GetInstitution()
        {
            string userId = User.Identity.GetUserId();

            string institution = "";
            var user = userBL.GetUser(userId);
            if (user != null && user.InstitutionId > 0)
            {
                var com = institutionBL.GetInstitution(user.InstitutionId);
                if (com != null)
                {
                    institution = com.Name;
                }

            }

            return institution;
        }

    }
}
