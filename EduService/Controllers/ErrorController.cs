﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EduService.Controllers
{
    public class ErrorController : Controller
    {
        // Return the 404 not found page   
        public ActionResult Error404()
        {
            return View();
        }
        // Return the 401 not authorized
        public ActionResult Error401()
        {
            return View();
        }
        // Return the 403 forbidden
        public ActionResult Error403()
        {
            return View();
        }
        // Return the 500 not found page   
        public ActionResult Error500()
        {
            string strRedirect = Server.UrlDecode(Request["ReturnUrl"]);

            if (!string.IsNullOrWhiteSpace(strRedirect))
            {
                return RedirectToAction("Error500", "Error");
            }
            return View();
        }             
    }
}
