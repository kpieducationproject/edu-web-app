﻿using CommonLibrary;
using EduService.BusinessLayer;
using EduService.Models;
using EduService.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EduService.Controllers
{
    public class TimetableController : Controller
    {

        EduServiceDBEntities db = new EduServiceDBEntities();
        TimetableLogic objTimeLogic = new TimetableLogic();
        SubjectBL objSubLogic = new SubjectBL();
        GradeBL objGradeLogic = new GradeBL();
        UsersBL userBL = new UsersBL();
    
        #region
        internal static ApplicationUserManager _userManager;
        internal static ApplicationSignInManager _signInManager;
        internal static ApplicationRoleManager _appRoleManager;
        EncryptDecryptQueryString encryptDecryptQueryString = new EncryptDecryptQueryString();
        public Cryptography _cryptography = new Cryptography();

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }
        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _appRoleManager ?? Request.GetOwinContext().GetUserManager<ApplicationRoleManager>();
            }
            private set
            {
                _appRoleManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        public string GetUserRoleByUserId(string IdNumber)
        {
            string role = "Unknown Role";

            var userExists = userBL.FindUserById(IdNumber);

            if (userExists != null)
            {
                var roles = UserManager.GetRoles(IdNumber);

                if (roles != null)
                {
                    role = roles.FirstOrDefault();
                }
            }

            return role;
        }
        #endregion

        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();

            var loggedInUser = userBL.GetUser(userId);
            
            List<TimetableViewModel> newTable = new List<TimetableViewModel>();
            
            newTable = objTimeLogic.getFullTimetable();

           

            return View(newTable);
        }


        public ActionResult TeacherTimetable()
        {
            string userId = User.Identity.GetUserId();

            var loggedInUser = userBL.GetUser(userId);

            List<TimetableViewModel> newTable = new List<TimetableViewModel>();
            
            newTable = objTimeLogic.getTimetableByTeacher(loggedInUser.Id, newTable);
            
            return View(newTable);

        }
        
        
        [Authorize(Roles = "HOD")]
        [HttpGet]
        public ActionResult Create()
        {
            TimetableViewModel table = new TimetableViewModel();

            table.Classes = PopulateClassDropDownList();
            table.Subjects = PopulateSubjectsDropDownList();
            table.Teachers = PopulateTeachersDropDownList();

            return View(table);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(TimetableViewModel lesson)
        {
            var result = string.Empty;

            if (ModelState.IsValid)
            {
                lesson.timetableDescription = objSubLogic.GetSubject(lesson.subjectId).subjectName + " at " + lesson.lessonTime;

                var check = db.tbl_Timetable.FirstOrDefault(x => x.lessonTime == lesson.lessonTime && x.ClassId == lesson.classId || x.lessonTime == lesson.lessonTime && x.teacherId == lesson.userId);

                if(check == null)
                {
                    var lessonId = objTimeLogic.Create(lesson);

                    if (lessonId > 0)
                    {
                        TempData["LessonCreationSuccessful"] = lesson.timetableDescription + " has been added successfully!";
                        return RedirectToAction("Index", "Timetable");
                    }
                    else
                    {
                        TempData["LessonCreationSuccessful"] = "Failed to add new lesson, please try again.";
                    }
                }
                else
                {
                    TempData["LessonCreationSuccessful"] = "A Lesson Already Exists for this teacher/class!";
                }
                
            }

            return View(lesson);
        }


        private IEnumerable<SelectListItem> PopulateSubjectsDropDownList()
        {
            CreateLessonViewModel lesson = new CreateLessonViewModel();
            var subjects = objSubLogic.GetAllSubjects();

            if(subjects.Count > 0)
            {
                lesson.Subjects = new SelectList(subjects.ToList(), "subjectId", "subjectName");
            }
            else
            {
                lesson.Subjects = Enumerable.Empty<SelectListItem>();
            }
            return lesson.Subjects;
        }

        private IEnumerable<SelectListItem> PopulateClassDropDownList()
        {
            CreateLessonViewModel lesson = new CreateLessonViewModel();
            var classes = db.tbl_Class.ToList();

            if (classes.Count > 0)
            {
                lesson.Classes = new SelectList(classes.ToList(), "Id", "Name");
            }
            else
            {
                lesson.Classes = Enumerable.Empty<SelectListItem>();
            }
            return lesson.Classes;
        }
        
        private IEnumerable<SelectListItem> PopulateTeachersDropDownList()
        {
            CreateLessonViewModel lesson = new CreateLessonViewModel();
            var teachers_temp = db.AspNetUsers.ToList();

            List<AspNetUsers> teachers = new List<AspNetUsers>();

            foreach(var item in teachers_temp)
            {
                if(GetUserRoleByUserId(item.Id) == "Teacher")
                {
                    teachers.Add(item);
                }
                else
                {

                }
            }

            if(teachers.Count > 0)
            {
                lesson.Teachers = new SelectList(teachers.ToList(), "Id", "FirstName");
            }
            else
            {
                lesson.Teachers = Enumerable.Empty<SelectListItem>();
            }
            return lesson.Teachers;
           
        }

        [Authorize(Roles = "HOD")]
        [HttpGet]
        [EncryptedActionParameter]
        public ActionResult Edit(int id)
        {
            var lesson = objTimeLogic.findLessonById(id);
            TimetableViewModel lessonvm = new TimetableViewModel();
            lessonvm.Classes = PopulateClassDropDownList();
            lessonvm.Subjects = PopulateSubjectsDropDownList();
            lessonvm.Teachers = PopulateTeachersDropDownList();

            if(lesson != null)
            {
                lessonvm.timetableId = lesson.Id;
                lessonvm.timetableDescription = lesson.timetableDescription;
                lessonvm.lessonTime = lesson.lessonTime;
                lessonvm.dayofweek = lesson.dayoftheweek;
                lessonvm.subjectId = lesson.subjectId;
                lessonvm.classId = lesson.ClassId;
                lessonvm.userId = lesson.teacherId;
            }
            else
            {
                return RedirectToAction("Index", "Timetable");
            }

            return View(lessonvm);

        }


        [Authorize(Roles = "HOD")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(TimetableViewModel lesson)
        {

            lesson.timetableDescription = objSubLogic.GetSubject(lesson.subjectId).subjectName + " at " + lesson.lessonTime;

            if (ModelState.IsValid)
            {
                bool updated = objTimeLogic.Edit(lesson);

                if (updated)
                {
                    TempData["updatedLesson"] = lesson.timetableDescription + " has been updated successfully";
                    return RedirectToAction("Index", "Timetable");
                }else
                {
                    ModelState.AddModelError("", "Failed to update. Please try again.");
                }
            }

            lesson.Subjects = PopulateSubjectsDropDownList();
            lesson.Classes = PopulateClassDropDownList();
            lesson.Teachers = PopulateTeachersDropDownList();

            return View(lesson);

        }

    }
}